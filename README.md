# Les mains dans le cambouis — un tremplin vers l'action

## Présentation

À l'échelle de la ville de Strasbourg, cette plateforme forme les citoyen·ne·s à contribuer aux initiatives sociales et environnementales de leur ville.

## Liens avec des projets similaires

Des projets similaires existent. S'il n'est pas réalisable de faire mieux, nous dédierons notre énergie à aider un projet existant.
Si nous parvenons à passer à une plus grande échelle, alors un des objectifs indispensables sera
de faire bénéficier les initiatives existantes de cette dynamique.

[Initiavites existantes](initiatives-existantes.md)


## Raison d'être

De nombreux problèmes de société sont traités par des associations et collectifs. Ces associations et collectifs réalisent un travail remarquable. Cependant, à l'heure où l'hiver arrive, il reste toujours de très nombreuses personnes à dormir à la rue, tandis que de nombreuses personnes n'arrivent pas à passer le cap et aller contribuer à résoudre ce problème.

Le rôle de cette plateforme est de faciliter le passage à l'action des citoyen·ne·s de la ville. Cela passe par la prise en compte des spécificités de chacun et chacune, en accompagnant au choix d'une ou plusieurs activités parmi celles mises en place au sein d'une des associations.

## Étapes

### Facilitation du passage à l'action

L'équipe de recrutement se déplace là où les gens sont, à l'heure où les gens sont disponibles.

Le premier objectif est de trouver sur quel sujet la personne désire s'engager mais n'a pas sauté le pas. Une proposition d'action avec une association est suggérée et une date fixée.

Avant cette date, un rendez-vous pour la formation expresse est fixé.

Par exemple, une personne qui passe tous les matins devant un sdf peut souhaiter participer à la distribution de repas. Ces distributions ont lieu chaque soir de la semaine. Un rendez-vous peut être fixé dans la semaine pour la formation expresse et un rendez-vous peut être fixé une semaine plus tard pour une distribution de repas.

### Formation expresse et mise en lien avec une association

Le parcours d'accompagnement de la plateforme est constitué de :
- un accompagnement au choix d'une action
- une initiation courte à la contribution à cette action.

Par exemple, une personne qui souhaite faire une distribution de repas sera informée des quelques petites choses à savoir pour cette action. Par ailleurs, une sensibilisation à la contribution à la réalisation de l'action (suggestion, prise de responsabilité, communication claire) lui est dispensée.

### Mise en lien avec une association

La personne formée est ensuite mise en lien avec une association qui réalise cette action.

### Suivi régulier

À un rythme régulier défini en lien avec les disponiblités de la personne, un rendez-vous régulier est fixé, afin de réaliser un suivi.

### Suivi régulier et polyactivité

Un rendez-vous est fixé dès la formation pour évoquer la possibilité de réaliser une autre action. Et donc d'être formé·e pour cette deuxième action.

La poursuite dans une action se fait en lien avec les besoins de l'association.

Il n'y a pas de limite sur les différentes actions qui peuvent être réalisées.



## Moyens développés pour assurer la dynamique globale

### Cartographie des acteurs

Une cartographie exhaustive des acteurs est réalisée en employant Communecter.org

### Référencement des actions

Par ailleurs, un référencement des actions réalisées auxquelles peuvent contribuer des citoyen·ne·s est tenu à jour. Cela inclue les dates et horaires des actions, ainsi que les besoins en nombre de personnes, etc.

### Contribution au développement des activités des associations

L'augmentation du nombre de contributeur·rice·s exerce un changement sur les associations. Par conséquent, l'équipe de la plateforme accompagne les associations à développer leurs activités.

Par exemple, une associations réalisant des distributions de repas peut mettre en place une tournée un soir de plus dans la semaine.


## Ce que cette plateforme ne fait pas

La plateforme n'a pas vocation à être une association réalisant une action pour une cause. La plateforme se limite à faciliter le passage à l'action, former et mettre en lien avec les associations.

## Quelle pérennité ?

Cette plateforme est basée sur du travail non rémunéré.

Elle a vocation à être intégrée à une dynamique de gouvernance et d'exécution municipale.

### Lien avec la gouvernance

Chaque action réalisée par un·e citoyen·ne est une prise de position sur un sujet qui mérite d'être prise en compte.

Avec l'accord des participant·e·s, chaque action sera décomptée comme un vote pour une mesure allant dans la direction de cette action.

### Lien avec l'exécutif

L'organe exécutif de la ville (la municipalité, au niveau d'une ville), devrait s'atteler à la résolution de ces problèmes de société. Cette plateforme a pour vocation de passer à une plateforme où les actions sont rémunérées sur un budjet municipal.


## Foire Aux Questions

- Y a-t-il un lien avec "Territoire Zéro Chômeurs de Longue Durée" ?

Pas directement, mais ces territoires sont une source d'inspiration. La plateforme vise en quelque sorte un territoire Zéro Inactivité. Tant qu'il y aura du monde à dormir dans la rue. Et un climat qui se dérégule.

